package dataAccessLayer;




import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Order;

@SuppressWarnings("serial")
public class OrderTable extends AbstractTableModel {
	
	
	
		public Object[][] data;	
		public String[] colNames={ "ID", "ClientID", "ProductID", "Quantity", "Product", "Cost"};
//			public ArrayList<Client> rows=new ArrayList<Client>(); 
			
			
		public OrderTable(List<Order> listaComenzi ){
				data=new Object[listaComenzi.size()][colNames.length];
				for(int i=0;i<listaComenzi.size(); i++)
				{int j=0;
				data[i][j]=listaComenzi.get(i).getIdOrder();
				j=1;
				data[i][j]=listaComenzi.get(i).getIdClient();
				j=2;
				data[i][j]=listaComenzi.get(i).getIdProduct();
				j=3;
				data[i][j]=listaComenzi.get(i).getQuantity();
				j=4;
				data[i][j]=listaComenzi.get(i).getProduct();
				j=5;
				data[i][j]=listaComenzi.get(i).getCost();
				
				}
			}
			
			public int getColumnCount(){
				return colNames.length;
				}
			
			public int getRowCount(){
				return data.length;
			}
			
			public String getColumnName(int col){
				return colNames[col];
					}
			
		
		public Object getValueAt(int row, int col) {
			// TODO Auto-generated method stub
			return data[row][col];
		}

}

