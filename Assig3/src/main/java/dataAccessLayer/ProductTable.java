package dataAccessLayer;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import model.Product;

public class ProductTable extends AbstractTableModel {
	
	
	
	public Object[][] data;	
	public String[] colNames={ "ID", "Product", "Cost", "Stock" };			
		
	public ProductTable(List<Product> listaProduse ){
			data=new Object[listaProduse.size()][colNames.length];
			for(int i=0;i<listaProduse.size(); i++)
			{int j=0;
			data[i][j]=listaProduse.get(i).getIdProduct();
			j=1;
			data[i][j]=listaProduse.get(i).getName();
			j=2;
			data[i][j]=listaProduse.get(i).getCost();
			j=3;
			data[i][j]=listaProduse.get(i).getStock();
			}
		}
		
		public int getColumnCount(){
			return colNames.length;
			}
		
		public int getRowCount(){
			return data.length;
		}
		
		public String getColumnName(int col){
			return colNames[col];
				}
		
	
	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

}
