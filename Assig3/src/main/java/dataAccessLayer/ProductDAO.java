package dataAccessLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnect;
import model.Product;

public class ProductDAO {
	private DBConnect db = DBConnect.instance();

	public List<Product> listAllProducts(){
		PreparedStatement findStatement = null;
		ResultSet result = null;

		List<Product> productList = new ArrayList<Product>();

		try {
			try {
				findStatement = db.sqlStatement("SELECT * FROM warehouse.product");

			}catch (Exception e) {
				e.printStackTrace();
			}
			result = findStatement.executeQuery();
			while (result.next()) {
				int id = result.getInt("idProduct");
				String name = result.getString("name");
				int cost = result.getInt("cost");
				int stock = result.getInt("stock");
				Product product = new Product (id, name, cost, stock);
				productList.add(product);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}


		return productList;
	}
	
	public int generateID() {
		PreparedStatement findStatement = null;
		ResultSet rez = null;
		int id = 0, idmax = 0;
		try {
			try {
				findStatement = db.sqlStatement("SELECT idProduct from warehouse.product");
				
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			rez = findStatement.executeQuery();
			while(rez.next()) {
				if(rez.getInt(1)>idmax)
					idmax = rez.getInt(1);
			}
		}catch(Exception e){
		}
		id = idmax + 1;
		return id;
		}
	
	private static final String insertStatementDef = "INSERT INTO warehouse.product (idProduct, name, cost, stock)"
			+ " VALUES (?, ?, ?, ?)";

	public void insertProduct(String name, int cost, int stock) {
		PreparedStatement insertStatement = null;
		
		try {
			insertStatement = db.sqlStatement(insertStatementDef);
			insertStatement.setInt(1, generateID());
			insertStatement.setString(2, name);
			insertStatement.setInt(3, cost);
			insertStatement.setInt(4, stock);
			insertStatement.execute();
			

		} catch (Exception e) {

		}
		
	}
	
	public void updateProduct(int idProduct, String name, int cost, int stock) {
		PreparedStatement updateStatement = null;
		
		try {
			updateStatement = db.sqlStatement(
			"UPDATE warehouse.product SET name = '"+name+"', cost = '"+cost+"', stock = '"+stock+"' WHERE idProduct = '"+idProduct+"'");
			updateStatement.execute();
		

		} catch (Exception e) {

		}
		
	}

	public boolean deleteProduct(int idProduct) {
		PreparedStatement deleteStatement = null;
		boolean done = false;
		try {
			deleteStatement = db.sqlStatement("DELETE FROM warehouse.product WHERE idProduct = '"+idProduct+"'");
			deleteStatement.execute();
			done = true;

		} catch (Exception e) {

		}
		return done;
	}
}
