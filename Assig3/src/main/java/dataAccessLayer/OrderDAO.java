package dataAccessLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnect;
import model.Order;

public class OrderDAO {
	private DBConnect db = DBConnect.instance();
	
	private final static String findAllOrdersStatementDef = "SELECT * FROM warehouse.order";
	//private final static String findAllClientsStatementDef = "SELECT * FROM warehouse.client";
	private final static String insertOrderStatementDef = "INSERT INTO warehouse.order (idOrder, idClient, product, quantity, cost, idProduct) VALUES (?, ?, ?, ?, ?, ?)";
	
	public int generateID() {
		PreparedStatement findStatement = null;
		ResultSet rez = null;
		int id = 0, idmax = 0;
		try {
			try {
				findStatement = db.sqlStatement("SELECT idOrder from warehouse.order");
				
				
			}catch(Exception e) {
				e.printStackTrace();
			}
			rez = findStatement.executeQuery();
			while(rez.next()) {
				if(rez.getInt(1)>idmax)
					idmax = rez.getInt(1);
			}
		}catch(Exception e){
		}
		id = idmax + 1;
		return id;
		}
	
	public List<Order> listAllOrders(){
		PreparedStatement findStatement = null;
		ResultSet result = null;
		
		List<Order> orderList = new ArrayList<Order>();
		
		try {
			try {
				findStatement = db.sqlStatement(findAllOrdersStatementDef);
			}catch(Exception e) {
				e.printStackTrace();
			}
			result = findStatement.executeQuery();
			while(result.next()) {
				int id = result.getInt("idOrder");
				int clientid = result.getInt("idClient");
				int productid = result.getInt("idProduct");
				int quantity = result.getInt("quantity");
				int cost = result.getInt("cost");
				String product = result.getString("product");
				
				Order order = new Order(id, clientid, productid, quantity, product, cost);
				orderList.add(order);
			}
			
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		return orderList;
	}
	public void insertOrder(String product, int quantity, int idClient) {
		PreparedStatement insertStatement = null;
		PreparedStatement findStatement = null;
		PreparedStatement updateStatement = null;
		ResultSet result = null;
		int orderCost = 0;
		int productStock = 0;
		int id = 0;
		int productCost = 0;
		try {
			try{
			findStatement = db.sqlStatement("SELECT * FROM warehouse.product WHERE name = '"+product+"'");
			}catch(Exception e){
				e.printStackTrace();
			}
			result = findStatement.executeQuery();
			while(result.next()){
				productCost = result.getInt("cost");
				productStock = result.getInt("stock");
				id = result.getInt("idProduct");
			}
			if(productStock>=quantity){
				productStock = productStock - quantity;
			}else{
				quantity = productStock;
				productStock = 0;
			}
			updateStatement = db.sqlStatement(
			"UPDATE warehouse.product SET stock = '"+productStock+"' WHERE idProduct = '"+id+"' ");
			updateStatement.execute();
			insertStatement = db.sqlStatement(insertOrderStatementDef);
			insertStatement.setInt(1,generateID());
			insertStatement.setInt(2, idClient);
			insertStatement.setString(3, product);
			insertStatement.setInt(4, quantity);
			orderCost = quantity*productCost;
			insertStatement.setInt(5, orderCost);
			insertStatement.setInt(6, id);
			insertStatement.execute();
			

		} catch (Exception e) {

		}
		
	}
	
	
	
	
	public boolean deleteOrder(int idOrder) {
		PreparedStatement deleteStatement = null;
		boolean done = false;
		try {
			deleteStatement = db.sqlStatement(
					"DELETE FROM warehouse.order WHERE idOrder = '" + idOrder+"'");

			deleteStatement.execute();
			done = true;

		} catch (Exception e) {

		}
		return done;
	}

}
