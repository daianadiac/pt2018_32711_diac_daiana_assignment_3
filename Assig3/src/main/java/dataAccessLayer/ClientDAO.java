package dataAccessLayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import connection.DBConnect;
import model.Client;


public class ClientDAO {
	
	DBConnect db = DBConnect.instance();
	
	public int generateID() {
	PreparedStatement findStatement = null;
	ResultSet rez = null;
	int id = 0, idmax = 0;
	try {
		try {
			findStatement = db.sqlStatement("SELECT idClient from warehouse.client");
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		rez = findStatement.executeQuery();
		while(rez.next()) {
			if(rez.getInt(1)>idmax)
				idmax = rez.getInt(1);
		}
	}catch(Exception e){
	}
	id = idmax + 1;
	return id;
	}
	public List<Integer> getAllIDS(){
		PreparedStatement findStatement = null;
		ResultSet rez = null;
		
		List<Integer> ids = new ArrayList<Integer>();
		try {
			try {
				findStatement = db.sqlStatement("SELECT idClient FROM warehouse.client");
			} catch (Exception e) {
				e.printStackTrace();
			}
			rez = findStatement.executeQuery();
			while (rez.next()) {
				int clientid = rez.getInt("idClient");
				
				ids.add(clientid);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ids;
	}
	
	public List<Client> listAllClients(){
		PreparedStatement findStatement = null;
		ResultSet rez = null;
		
		List<Client> clients = new ArrayList<Client>();
		
		try {
			try {
				findStatement = db.sqlStatement("SELECT * FROM warehouse.client");
			} catch (Exception e) {
				e.printStackTrace();
			}
			rez = findStatement.executeQuery();
			while (rez.next()) {
				int clientid = rez.getInt("idClient");
				String name = rez.getString("name");
				
				Client client = new Client(clientid, name);
				clients.add(client);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clients;
	}
	
	public void insertClient( String name) {
		PreparedStatement insertStatement = null;
		try {
			insertStatement = db.sqlStatement("INSERT INTO warehouse.client (idClient, name)"
					+ "VALUES (?,?)");
			insertStatement.setInt(1, generateID());
			insertStatement.setString(2, name);
			
			insertStatement.execute();
		} catch(Exception e) {
			
		}
	}
	public ResultSet findByName (String name) {

		ResultSet rez = null;
		String find = "SELECT * FROM warehouse.client WHERE name = ?";
		PreparedStatement findStatement = null;
		try {
			findStatement = db.sqlStatement(find);
			findStatement.setString(1, name);
			rez = findStatement.executeQuery();
			
		} catch (Exception e) {
			
		}
		return rez;
		
	}
	public void updatetClient(int idClient, String name) {
		PreparedStatement insertStatement = null;
		try {
			insertStatement = db.sqlStatement("UPDATE warehouse.client SET name =' "
					+ "VALUES (?,?)");
			insertStatement.setInt(1, idClient);
			insertStatement.setString(2, name);
			
			insertStatement.execute();
		} catch(Exception e) {
			
		}
	}
	public boolean delete(int idClient) {
		PreparedStatement deleteStatement = null;
		boolean done = false;
		try {
			deleteStatement = db.sqlStatement("DELETE FROM warehouse.client WHERE idClient='"+ idClient +"' ");
			deleteStatement.execute();
			done = true;

		} catch (Exception e) {

		}
		return done;
	}
}
