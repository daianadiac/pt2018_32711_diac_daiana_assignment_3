package connection;



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

/**
 * DATABASE connection class realized with SINGLETON DESIGN PATTERN
 **/

public class DBConnect {

	/**
	 * connection settings
	 **/

	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String DBURL = "jdbc:mysql://localhost:3306/warehouse?autoReconnect=true&useSSL=false";
	public static final String DBNAME = "warehouse";
	private static String USER = "Daiana";
	private static String PASS = "root";

	private Statement instruction;
	private Connection con;

	/**
	 * singleton object
	 */
	private static DBConnect oneObject = null;
	
	private DBConnect(String database) {
		try {

			Class.forName(DRIVER);
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/warehouse?autoReconnect=true&useSSL=false", USER, PASS);
			//con = DriverManager.getConnection(DBURL , USER, PASS);
			instruction = con.createStatement();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Creates a unique instance of the database connection
	 * @return
	 */
	public static DBConnect instance() {
		if (oneObject == null)
			oneObject = new DBConnect(DBNAME);
		return oneObject;

	}
	/**
	 * closes the connection
	 */
	public void closeConnection() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * overrides method
	 * 
	 * @param querry
	 *            --query to be executed
	 * @return the results of the query
	 */
	public ResultSet executeQuerry(String querry) {
		ResultSet ret = null;
		try {
			ret = instruction.executeQuery(querry);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
	/**
	 * overrides method
	 * 
	 * @param querry
	 *            query to be executed
	 * @return if the query was successful
	 */
	public boolean execute(String querry) {
		boolean done = false;
		try {
			done = instruction.execute(querry);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return done;
	}
	public PreparedStatement sqlStatement(String query) throws Exception{
        return con.prepareStatement(query);
    }
}
