package model;

public class Order {
	//idOrder, idClient, product, quantity, cost , idProduct
	
	private int idOrder, idClient, idProduct, quantity;
	private String product;
	private int cost;
	
	public Order(int idOrder, int idClient, int idProduct, int quantity, String product, int cost) {
		this.idOrder = idOrder;
		this.idClient = idClient;
		this.idProduct = idProduct;
		this.quantity = quantity;
		this.product = product;
		this.cost = cost;
	}
	
	public Order() {
		
	}

	public int getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(int idOrder) {
		this.idOrder = idOrder;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
