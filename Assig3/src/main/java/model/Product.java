package model;

public class Product {
	//id, name, cost, stock
	
	private int idProduct, stock;
	private String name;
	private int cost;
	
	public Product(int idProduct, String name, int cost,  int stock ) {
		this.idProduct = idProduct;
		this.name = name;
		this.cost = cost;
		this.stock = stock;
	}
	
	public Product() {
		
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	
}
