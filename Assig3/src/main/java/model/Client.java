package model;

public class Client {
	//id, name
	
	private int idClient;
	private String name;
	
	public Client(int idClient, String name) {
		this.idClient = idClient;
		this.name = name;
	}
	
	public Client() {
		
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
