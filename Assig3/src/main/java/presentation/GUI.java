package presentation;

import java.awt.*;

import javax.swing.*;

import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.CardLayout;
import java.util.List;

import business_logic.AdminActions;
import model.Client;

import java.util.List;


import model.Product;
import model.Order;

import dataAccessLayer.OrderDAO;
import dataAccessLayer.OrderTable;
import dataAccessLayer.ProductDAO;
import dataAccessLayer.ProductTable;

public class GUI {

	private JFrame frmTema;
	private JTextField addProdus;
	private JTextField addPret;
	private JTextField addStoc;
	private JTextField updateID;
	private JTextField updatePret;
	private JTextField updateStoc;
	private JTextField updateProdus;
	private JTextField deleteID;
	private JPanel administratorPanel = new JPanel();
	private JPanel panelMeniu = new JPanel();
	private JPanel clientPanel = new JPanel();
	private JPanel newClient = new JPanel();
	private JLabel status = new JLabel("");
	private JTextField numeClient;
	private JLabel order = new JLabel("");
	private JScrollPane scrollPane;
	private JScrollPane scrollPane2;
	private JScrollPane scrollPane3;
	
	
	AdminActions admin = new AdminActions();
	List<Product> produs = null;
	List<Order> comenzi = null;
	List<Client> client = null;
	private JTextField clientAddProdus;
	private JTextField clientAddCantitate;
	private JTextField clientDelete;
	private JTable table;
	private JTable table_2;
	private JTable table_3;
	private JTextField textFieldidClientorder;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmTema.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public GUI() {
		initialize();
	}

	//Metoda de afisare in tabel a produselor in meniul administrator
	
	public void afisareInTabelProdus(List<Product> pp1)
	{
		//ArrayList<Produs> p1 = new ArrayList<Produs>();
		ProductTable pr1= new ProductTable(pp1);
		table = new JTable(pr1);
		
		scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 29, 414, 243);
	table.setPreferredScrollableViewportSize(new Dimension(400, 100));
		table.setFillsViewportHeight(true);
		administratorPanel.add( scrollPane );

	}

	//Metoda de afisare in tabel a produselor in meniul client
	
	public void afisareInTabelComenziProduse(List<Product> pp1){
		ProductTable pr1 = new ProductTable(pp1);
		table_2 = new JTable(pr1);
		
		scrollPane2 = new JScrollPane(table_2);
		scrollPane2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		scrollPane2.setBounds(10, 29, 414, 97);
		table_2.setPreferredScrollableViewportSize(new Dimension(420, 100));
		table_2.setFillsViewportHeight(true);
		scrollPane2.setVisible(true);
		clientPanel.add(scrollPane2);
	}
	
	//Metoda de afisare in tabel a comenzilor in meniul client
	
	public void afisareInTabelComenzi(List<Order> pp1){
		OrderTable pr1= new OrderTable(pp1);
		table_3 = new JTable(pr1);
		
		scrollPane3 = new JScrollPane(table_3);
		scrollPane3.setBounds(10, 175, 414, 97);
		table_3.setPreferredScrollableViewportSize(new Dimension(400, 100));
		table_3.setFillsViewportHeight(true);
		clientPanel.add( scrollPane3 );

		
	}
	
	private void initialize() {
		frmTema = new JFrame();
		frmTema.setTitle("Warehouse App");
		frmTema.setBounds(100, 100, 450, 447);
		frmTema.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTema.getContentPane().setLayout(new CardLayout(0, 0));
		
		
		frmTema.getContentPane().add(panelMeniu, "name_1046730571624134");
		panelMeniu.setLayout(null);
		panelMeniu.setVisible(true);
		administratorPanel.setVisible(false);
		clientPanel.setVisible(false);
		newClient.setVisible(false);
		
		frmTema.getContentPane().add(administratorPanel, "name_1046735125515848");
		
		JButton btnAdd = new JButton("ADD");
		btnAdd.setBounds(10, 307, 89, 23);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				produs = admin.addProduct(
				 
								  addProdus.getText(),
				Integer.parseInt(addPret.getText()), 
				Integer.parseInt(addStoc.getText()));
				status.setText("Added!");
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, "Fields incorrect!");
				}
			}
		});
		administratorPanel.setLayout(null);
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		administratorPanel.add(btnAdd);
		
		JButton btnUpdate = new JButton("UPDATE");
		btnUpdate.setBounds(10, 341, 89, 23);
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					produs = admin.updateProduct(Integer.parseInt(updateID.getText()), updateProdus.getText(), Integer.parseInt(updatePret.getText()), Integer.parseInt(updateStoc.getText()));
					status.setText("Updated!");
					}catch(Exception e1){
						JOptionPane.showMessageDialog(null, "Fields incorrect!");
					}
				
			}
		});
		btnUpdate.setFont(new Font("Tahoma", Font.BOLD, 11));
		administratorPanel.add(btnUpdate);
		
		JButton btnDelete = new JButton("DELETE");
		btnDelete.setBounds(10, 375, 89, 23);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					produs = admin.deleteProduct(Integer.parseInt(deleteID.getText()));
					status.setText("Deleted!");
				}catch(Exception e1){
					JOptionPane.showMessageDialog(null, "Fields incorrect!");
				}
				
			}
		});
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 11));
		administratorPanel.add(btnDelete);
		
		JButton btnBackAdmin = new JButton("BACK");
		btnBackAdmin.setBounds(316, 375, 108, 23);
		btnBackAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				administratorPanel.setVisible(false);
				panelMeniu.setVisible(true);
			}
		});
		btnBackAdmin.setFont(new Font("Tahoma", Font.BOLD, 11));
		administratorPanel.add(btnBackAdmin);
		
		addProdus = new JTextField();
		addProdus.setBounds(180, 308, 102, 20);
		administratorPanel.add(addProdus);
		addProdus.setColumns(10);
		
		addPret = new JTextField();
		addPret.setBounds(292, 308, 61, 20);
		addPret.setColumns(10);
		administratorPanel.add(addPret);
		
		addStoc = new JTextField();
		addStoc.setBounds(363, 308, 61, 20);
		addStoc.setColumns(10);
		administratorPanel.add(addStoc);
		
		updateID = new JTextField();
		updateID.setBounds(109, 342, 61, 20);
		updateID.setColumns(10);
		administratorPanel.add(updateID);
		
		updatePret = new JTextField();
		updatePret.setBounds(292, 342, 61, 20);
		updatePret.setColumns(10);
		administratorPanel.add(updatePret);
		
		updateStoc = new JTextField();
		updateStoc.setBounds(363, 342, 61, 20);
		updateStoc.setColumns(10);
		administratorPanel.add(updateStoc);
		
		updateProdus = new JTextField();
		updateProdus.setBounds(180, 342, 102, 20);
		updateProdus.setColumns(10);
		administratorPanel.add(updateProdus);
		
		deleteID = new JTextField();
		deleteID.setBounds(109, 376, 61, 20);
		deleteID.setColumns(10);
		administratorPanel.add(deleteID);
		
		JButton btnRefreshAdmin = new JButton("REFRESH");
		btnRefreshAdmin.setBounds(190, 375, 108, 23);
		btnRefreshAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProductDAO x = new ProductDAO();
				afisareInTabelProdus(x.listAllProducts());
				status.setText("Refreshed!");
			}
		});
		btnRefreshAdmin.setFont(new Font("Tahoma", Font.BOLD, 11));
		administratorPanel.add(btnRefreshAdmin);
		
		JLabel lblId = new JLabel("ID");
		lblId.setBounds(109, 283, 61, 14);
		lblId.setHorizontalAlignment(SwingConstants.CENTER);
		administratorPanel.add(lblId);
		
		JLabel lblPret = new JLabel("Cost");
		lblPret.setBounds(292, 283, 61, 14);
		lblPret.setHorizontalAlignment(SwingConstants.CENTER);
		administratorPanel.add(lblPret);
		
		JLabel lblStoc = new JLabel("Stock");
		lblStoc.setBounds(363, 283, 61, 14);
		lblStoc.setHorizontalAlignment(SwingConstants.CENTER);
		administratorPanel.add(lblStoc);
		
		JLabel lblProdus = new JLabel("Product");
		lblProdus.setBounds(180, 283, 102, 14);
		lblProdus.setHorizontalAlignment(SwingConstants.CENTER);
		administratorPanel.add(lblProdus);
		status.setFont(new Font("Tahoma", Font.BOLD, 11));
		status.setBounds(109, 316, 53, 14);
		status.setHorizontalAlignment(SwingConstants.CENTER);
		administratorPanel.add(status);
		
		table = new JTable();
		table.setBounds(10, 29, 414, 243);
		administratorPanel.add(table);
		
		JLabel lblProduse = new JLabel("Produse");
		lblProduse.setHorizontalAlignment(SwingConstants.CENTER);
		lblProduse.setBounds(10, 11, 414, 14);
		administratorPanel.add(lblProduse);
		
		JButton btnNewButton = new JButton("ORDER");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				administratorPanel.setVisible(false);
				clientPanel.setVisible(true);
			}
		});
		btnNewButton.setBounds(10, 277, 89, 23);
		administratorPanel.add(btnNewButton);
		
	
		frmTema.getContentPane().add(clientPanel, "name_1046737365354813");
		clientPanel.setLayout(null);
		
		JButton btnRefresh = new JButton("REFRESH");
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OrderDAO og = new OrderDAO();
				afisareInTabelComenzi(og.listAllOrders());
				ProductDAO x = new ProductDAO();
				afisareInTabelComenziProduse(x.listAllProducts());
				//pretFactura.setText(" "+ og.pretFinal());
				order.setText("Refreshed!");
			}
		});
		
		btnRefresh.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRefresh.setBounds(12, 283, 108, 23);
		clientPanel.add(btnRefresh);
		
		JButton btnClientAdd = new JButton("ADD");
		btnClientAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comenzi = admin.addOrder(clientAddProdus.getText(), Integer.parseInt(clientAddCantitate.getText()), Integer.parseInt(textFieldidClientorder.getText()));
				order.setText("Added!");
			}
		});
		btnClientAdd.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnClientAdd.setBounds(10, 341, 89, 23);
		clientPanel.add(btnClientAdd);
		
		JButton btnClientDel = new JButton("DELETE");
		btnClientDel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comenzi = admin.deleteOrder(Integer.parseInt(clientDelete.getText()));
				order.setText("Deleted!");
			}
		});
		btnClientDel.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnClientDel.setBounds(10, 375, 89, 23);
		clientPanel.add(btnClientDel);
		
		JButton anotherBack = new JButton("BACK");
		anotherBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clientPanel.setVisible(false);
				panelMeniu.setVisible(true);
			}
		});
		anotherBack.setFont(new Font("Tahoma", Font.BOLD, 11));
		anotherBack.setBounds(316, 375, 108, 23);
		clientPanel.add(anotherBack);
		
		JLabel lblProdus_1 = new JLabel("Product");
		lblProdus_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblProdus_1.setBounds(109, 312, 85, 14);
		clientPanel.add(lblProdus_1);
		
		JLabel lblCantitate = new JLabel("Quantity");
		lblCantitate.setHorizontalAlignment(SwingConstants.CENTER);
		lblCantitate.setBounds(204, 312, 89, 14);
		clientPanel.add(lblCantitate);
		
		JLabel label_4 = new JLabel("");
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setBounds(10, 283, 89, 14);
		clientPanel.add(label_4);
		
		JLabel lblIdOrder = new JLabel("ID Order:");
		lblIdOrder.setHorizontalAlignment(SwingConstants.CENTER);
		lblIdOrder.setBounds(109, 376, 85, 23);
		clientPanel.add(lblIdOrder);
		
		clientAddProdus = new JTextField();
		clientAddProdus.setBounds(109, 343, 89, 20);
		clientPanel.add(clientAddProdus);
		clientAddProdus.setColumns(10);
		
		clientAddCantitate = new JTextField();
		clientAddCantitate.setColumns(10);
		clientAddCantitate.setBounds(204, 343, 89, 20);
		clientPanel.add(clientAddCantitate);
		
		clientDelete = new JTextField();
		clientDelete.setColumns(10);
		clientDelete.setBounds(204, 377, 89, 20);
		clientPanel.add(clientDelete);
		
		JLabel lblNewLabel_1 = new JLabel("Client ID");
		lblNewLabel_1.setBounds(316, 312, 46, 14);
		clientPanel.add(lblNewLabel_1);
		
		
		order.setBounds(10, 312, 89, 14);
		clientPanel.add(order);
		
		table_2 = new JTable();
		table_2.setBounds(10, 29, 414, 97);
		clientPanel.add(table_2);
		
		table_3 = new JTable();
		table_3.setBounds(10, 175, 414, 97);
		clientPanel.add(table_3);
		
		JLabel lblProducts = new JLabel("Products:");
		lblProducts.setBounds(10, 11, 74, 14);
		clientPanel.add(lblProducts);
		
		JLabel lblBasket = new JLabel("Basket:");
		lblBasket.setBounds(10, 161, 74, 14);
		clientPanel.add(lblBasket);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 124, 414, -92);
		clientPanel.add(scrollPane_1);
		
		textFieldidClientorder = new JTextField();
		textFieldidClientorder.setBounds(306, 341, 98, 20);
		clientPanel.add(textFieldidClientorder);
		
		JButton btnAdministrator = new JButton("Administrator");
		btnAdministrator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				administratorPanel.setVisible(true);
				panelMeniu.setVisible(false);
			}
		});
		btnAdministrator.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnAdministrator.setBounds(257, 137, 139, 61);
		panelMeniu.add(btnAdministrator);
		
		JButton btnClient = new JButton("Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newClient.setVisible(true);
				panelMeniu.setVisible(false);
			}
		});
		btnClient.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnClient.setBounds(47, 137, 139, 61);
		panelMeniu.add(btnClient);
		
		JButton btnExit = new JButton("Exit!");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnExit.setBounds(147, 300, 139, 61);
		panelMeniu.add(btnExit);
		
		JLabel lblNewLabel = new JLabel("Choose User:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(149, 34, 137, 44);
		panelMeniu.add(lblNewLabel);
		
		
		frmTema.getContentPane().add(newClient, "name_1051348949709391");
		newClient.setLayout(null);
		
		numeClient = new JTextField();
		numeClient.setBounds(228, 93, 133, 20);
		newClient.add(numeClient);
		numeClient.setColumns(10);
		
		JButton btnJustOK = new JButton("OK!");
		btnJustOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				client = admin.addClient(numeClient.getText());
			}
		});
		btnJustOK.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnJustOK.setBounds(152, 218, 113, 47);
		newClient.add(btnJustOK);
		
		JButton btnJustBack = new JButton("Back");
		btnJustBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newClient.setVisible(false);
				panelMeniu.setVisible(true);
			}
		});
		btnJustBack.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnJustBack.setBounds(164, 286, 89, 47);
		newClient.add(btnJustBack);
		
		JLabel lblInsertName = new JLabel("Insert name:");
		lblInsertName.setBounds(98, 95, 89, 16);
		newClient.add(lblInsertName);
		
		}
}
