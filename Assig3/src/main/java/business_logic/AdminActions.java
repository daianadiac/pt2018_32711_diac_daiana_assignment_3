package business_logic;

import java.sql.ResultSet;
import java.util.List;

import dataAccessLayer.ClientDAO;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Client;
import model.Order;
import model.Product;

public class AdminActions {
	
	ClientDAO clientdao = new ClientDAO();
	ProductDAO productdao = new ProductDAO();
	OrderDAO orderdao = new OrderDAO();
	
	/*public void listAll(List<Object> list) {
		for(Object a:list) {
			System.out.println(a);
		}
	}*/
	
	public ResultSet findName(String name) {
		ResultSet rez = null;
		try {
			rez = clientdao.findByName(name);
			
		}catch (Exception e) {
			
		}
		return rez;
	}
	public List<Product> addProduct( String name, int cost, int stock){
		List<Product> products = null;
		
		productdao.insertProduct( name, cost, stock);
		products = productdao.listAllProducts();
		return products;
	}
	
	public List<Product> deleteProduct(int idProduct){
		List<Product> products = null;
		productdao.deleteProduct(idProduct);
		products = productdao.listAllProducts();
		return products;
	}
	
	public List<Product> updateProduct(int idProduct, String name, int cost, int stock){
		List<Product> products = null;
		productdao.updateProduct(idProduct, name, cost, stock);
		products = productdao.listAllProducts();
		return products;
	}
	
	public List<Integer> getAllClientIDS(){
		List<Integer> clientids = null;
		clientids = clientdao.getAllIDS();
		return clientids;
	}
	
	public List<Client> addClient(String name){
		List<Client> clients = null;
		clientdao.insertClient(name);
		clients = clientdao.listAllClients();
		return clients;
	}
	
	public List<Client> deleteClient(int idClient){
		List<Client> clients = null;
		clientdao.delete(idClient);
		clients = clientdao.listAllClients();
		return clients;
	}
	
	public List<Client> updateClient(int idClient, String name){
		List<Client> clients = null;
		clientdao.updatetClient(idClient, name);
		clients = clientdao.listAllClients();
		return clients;
	}
	
	public List<Order> addOrder(String product, int quantity, int idClient){
		List<Order> orders = null;
		orderdao.insertOrder(product, quantity, idClient);
		orders =orderdao.listAllOrders();
		return orders;
	}
	
	public List<Order> deleteOrder(int idOrder){
		List<Order> orders = null;
		orderdao.deleteOrder(idOrder);
		orders =orderdao.listAllOrders();
		return orders;
	}
	

}
